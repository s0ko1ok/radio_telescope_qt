#include "new_mainwindow.h"

#include <QPushButton>
#include <QVBoxLayout>

NewMainWindow::NewMainWindow(QWidget *parent) : QWidget(parent)
{
    QVBoxLayout * mainLayout = new QVBoxLayout(this);
    setLayout(mainLayout);
    QPushButton *btn = new QPushButton("test");
    mainLayout->addWidget(btn);
}
