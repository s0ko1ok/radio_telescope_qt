#include "comportthread.h"
#include "coordinatesview.h"
#include  <QDebug>
#include <QSerialPort>
#include <QTextCodec>
#include "radioskycanvas.h"
ComPortThread::ComPortThread(QString threadName,QSerialPort *Serialp,CoordinatesView *CoordinatesView)
{
 serial = Serialp;
 name =threadName;
 coordinatesView = CoordinatesView;
 qDebug() << "start timer";
 startTimer(100);
}

void ComPortThread::run()
{

//   while(stop){
//     getCoordinates();
//     if (comanndQueue.empty())
//        QThread::msleep(100);
//     else{
//         sendCommands(comanndQueue.front());
//         comanndQueue.pop();
//     }
//   }
//getCoordinates();

}
void ComPortThread::timerEvent(QTimerEvent* e){
    doIt();

}
void ComPortThread::doIt(){
//     qDebug() << "doIt";
    getCoordinates();
    if (!comanndQueue.empty())
      {
        sendCommands(comanndQueue.front());
        comanndQueue.pop();
    }
}
QByteArray ComPortThread::send(QByteArray br){
    serial -> write(br);
    usleep(100000);
    QByteArray responseData;
    return serial -> readAll();

}
void ComPortThread::sendCommands(QString string){

    QByteArray br = string.toUtf8();
    int DyteArraySize = br.size();
    br[DyteArraySize-1]= 13;


    qDebug() <<"send" <<  string;
    QByteArray responseData = send(br);

     qDebug() << responseData;
     if (responseData == ""){
         qDebug() << "repeat1" ;
         usleep(150000);
         responseData = send(br);
     }
     if (responseData == ""){
         qDebug() << "repeat2" ;
         usleep(150000);
         responseData = send(br);
     }
     if (responseData == ""){
         qDebug() << "repeat3" ;
         usleep(150000);
         responseData = send(br);
     }
    setComandReturnStatus(responseData);

//    if (serial -> waitForReadyRead(1000)) {
//         responseData = serial -> readAll();
//        while (serial -> waitForReadyRead(10))
//            responseData += serial -> readAll();

//        qDebug() << responseData;
//         emit setComandReturnStatus(responseData);
//    }

}
void ComPortThread::setCoordinates(Coordinates coordinates){
    coordinates.getAzimut();
    coordinates.getElevation();
    QString string;
    putComand( string);
}
void ComPortThread::getCoordinates(){

//qDebug() << __func__;
    QByteArray requestData2;
    requestData2.resize(2);
    requestData2[0] = 89;
    requestData2[1] = 13;


        serial -> write(requestData2);
        usleep(10000);
        QByteArray responseData = serial->readAll();
#if 0
        int numRead = 0;
        char responseData[50];
        numRead  = serial->read(responseData, 50);
        responseData[numRead] = '\0';
        //QByteArray responseData = serial->readAll();
        if (numRead < 2 || responseData[0] != 'O' | responseData[1] != 'K'){
            qDebug()  << "alert!";
            //exit(1);
            return;
        }
#endif
//        qDebug() << responseData;
        if (responseData.size() > 0){
            emit this->has_data(responseData);
        }
/*
    int numRead = 0;
     char buffer[50];

     for (;;) {
         numRead  = serial->read(buffer, 50);

         // Do whatever with the array
         buffer[numRead] = '\0';
         qDebug() << buffer;

         if (numRead == 0 && !serial->waitForReadyRead())
             break;
     }
*/
#if 0
    QByteArray responseData = serial -> readAll();
    if (responseData == "ACK\r"){
        qDebug()  << "alert!";
        exit(1);
    }


//     QByteArray responseData;
    QByteArray responseData = serial -> readAll();
    if (responseData == "ACK\r"){
        qDebug()  << "alert!";
        exit(1);
    }
    qDebug()  << 3 << responseData;
    if (serial -> waitForReadyRead(1000)) {
        QByteArray responseData = serial -> readAll();
        while (serial -> waitForReadyRead(10))
            responseData += serial -> readAll();

        qDebug() << 4 << responseData;
        //Coordinates coo = Coordinates((QString)responseData);
        //coordinatesView->setCoordinates(coo);
        //canvas->drawCoo(coo);
        emit this->has_data(responseData);
    }
    emit this->has_data(responseData);
#endif
}
void ComPortThread::putComand(QString command ){
    qDebug() << "put command" << command;
    comanndQueue.push(command);

}
