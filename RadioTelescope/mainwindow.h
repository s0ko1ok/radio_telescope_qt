    #ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "fiderthread.h"
#include "radioskycanvas.h"
#include "scanskythread.h"
#include <QtWebSockets/QWebSocket>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QMainWindow>
#include "chart_detector.h"
#include "radiosky_graphics_view.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    RadioSkyCanvas *canvas ;
//    CoordinatesView *currentCoordinatesView;
    ScanSkyThread *scanSkyThread = nullptr;
    FiderThread * fiderThread ;
    ChartDetector * chartDetector;
    QTimer * timer ;
    QWidget * centralwidget;
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    QComboBox * comPortListComboBox;
    QSerialPort serial;
    void getCOMPorts();
    void sendTestData();
    void delay(int);
    void OpenComPort();
    void drawRadioSky();
    void stopScan();

    QPushButton * pushButton_Scan;

    QLabel * cursorAzimut;
    QLabel * cursorElevation;
    QLabel * setAzimut;
    QLabel * setElevation;
    QLabel * currentAzimut;
    QLabel * currentElevation;

    CoordinatesView * cursorCoordinatesView;
    CoordinatesView * setCoordinatesView;
    CoordinatesView * currentCoordinatesView;


    QLabel * sendComandLabel;
    QLabel * comandLabel;
    QLabel * comPortStatus;
    QLabel * commandReturn;
    QLabel * detectorValLabel;
    QLabel * detectorValIntLabel;
    QVBoxLayout * radioSkyLayout ;
    RadioSkyGraphicsView * graphicsView;

public slots:
    void setDetetorValue(double val);
     void sendDataToComPort(QString command);
signals:
    void scanTerminate();
    void on_pushButton_clickedS();

protected:
    void createRainBlow();

private slots:
    void on_pushButton_clicked();
    void startComPortThread(QSerialPort *Serialp);
    void on_comboBox_COM_port_activated(const QString &arg1);
    void hasData(const QString &s);
    void ComPortIsOpen();
    void setComandReturnStatus(const QString &strr);
    void stateChangedSlot(QAbstractSocket::SocketState state);


    void on_pushButton_Scan_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::MainWindow *ui;
    ComPortThread *comPortThread;
};
#endif // MAINWINDOW_H
