#ifndef SCANSKYTHREAD_H
#define SCANSKYTHREAD_H
#include "coordinates.h"

#include <QThread>
#include <QTimer>
#include <queue>
#include <bits/stl_queue.h>
#include <QString>

extern Coordinates g_current_coordinates;
extern bool g_current_coordinates_Mutex;
class ScanSkyThread : public QThread
{
    Q_OBJECT
public:
    ScanSkyThread();
    ScanSkyThread(QString threadName);
    void run();
    void setAntennaDirection(double Azimut, double Elevation);

    int sector = 0;
    int azimytN ;
    double elevationN;
public slots:
    void slotTimerAlarm();
signals:
    void startCoordinate( QString stringg);
private:
    void timerEvent(QTimerEvent *e);
};

#endif // SCANSKYTHREAD_H
