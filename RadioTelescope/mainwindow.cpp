#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QTime>
#include <QPainter>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include "comportthread.h"
#include "fiderthread.h"
#include "new_mainwindow.h"
#include "radioskycanvas.h"
#include "scanskythread.h"
#include "websocketclient.h"
double detectorValD;
int32_t detectorValI;
extern WebSocketClient *client;
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , comPortThread(nullptr)
{
#if 01
    if (objectName().isEmpty())
        setObjectName(QString::fromUtf8("MainWindow"));
    resize(987, 856);

    centralwidget = new QWidget(this);
    centralwidget->setObjectName(QString::fromUtf8("centralwidget"));

    QHBoxLayout * mainLayout = new QHBoxLayout(this);
    centralwidget->setLayout(mainLayout);
    QSplitter * splitter = new QSplitter(this);

    mainLayout->addWidget(splitter);

    QTabWidget *leftBar = new QTabWidget(splitter);
    QTabWidget *mainBar = new QTabWidget(splitter);

    leftBar->setMaximumWidth(350);
    leftBar->setMinimumWidth(350);

    QWidget * controlWidget = new QWidget();
    QVBoxLayout * controlWidgetLayout = new QVBoxLayout();





    cursorCoordinatesView = new CoordinatesView("Cursor coordinate");
    setCoordinatesView = new CoordinatesView("Set coordinate");
    currentCoordinatesView = new CoordinatesView("Current coordinate");

    QGroupBox * comportBox = new QGroupBox("ComPortControl");
    QFormLayout * comportBoxLayout = new QFormLayout();

    sendComandLabel = new QLabel("sendComandLabel");
    comandLabel = new QLabel("comandLabel");
    comPortStatus = new QLabel("comPortStatus");
    commandReturn = new QLabel("commandReturn");
    comPortListComboBox = new QComboBox();
    comportBoxLayout->addRow("Comport select",comPortListComboBox);
    comportBoxLayout->addRow("sendComandLabel",sendComandLabel);
    comportBoxLayout->addRow("comandLabel",comandLabel);
    comportBoxLayout->addRow("comPortStatus",comPortStatus);
    comportBoxLayout->addRow("commandReturn",commandReturn);
    comportBox->setLayout(comportBoxLayout);






    controlWidgetLayout->addWidget(comportBox,1);



    pushButton_Scan = new QPushButton("scan");
    controlWidgetLayout->addWidget(pushButton_Scan,1);

    controlWidgetLayout->addWidget(cursorCoordinatesView,1);
    controlWidgetLayout->addWidget(setCoordinatesView,1);
    controlWidgetLayout->addWidget(currentCoordinatesView,1);

    QGroupBox * detectorBox = new QGroupBox("Detector");
    QFormLayout * detectorBoxLayout = new QFormLayout();
    detectorValIntLabel = new QLabel("detectorValIntLabel");
    detectorValLabel = new QLabel("detectorValLabel");
    detectorBoxLayout->addRow("Tnteger val:",detectorValIntLabel);
    detectorBoxLayout->addRow("Voltage:", detectorValLabel);
    detectorBox->setLayout(detectorBoxLayout);
    controlWidgetLayout->addWidget(detectorBox,1);
    controlWidgetLayout->setSpacing(3);
    controlWidgetLayout->addStretch(1);
    controlWidget->setLayout(controlWidgetLayout);

    QWidget * settingsWidget = new QWidget();
    QVBoxLayout * settingsWidgetLayout = new QVBoxLayout();
    settingsWidget->setLayout(settingsWidgetLayout);



    QWidget * radioSkyWidget = new QWidget();
    radioSkyLayout = new QVBoxLayout();
    radioSkyWidget -> setLayout(radioSkyLayout);

    QWidget * skyFragmentWidget = new QWidget();
    QWidget * detectorWidget = new QWidget();

    QVBoxLayout * mainVDetectorLayout = new  QVBoxLayout();
    detectorWidget->setLayout(mainVDetectorLayout);


    leftBar->addTab(controlWidget,"control");
    leftBar->addTab(settingsWidget,"Settings");

    chartDetector = new ChartDetector();
//    QVBoxLayout * detectorWidgetLayout = new QVBoxLayout();
    mainVDetectorLayout->addWidget(chartDetector);
//    detectorWidget->setLayout(detectorWidgetLayout);

    mainBar->addTab(radioSkyWidget,"RadioSky");
    mainBar->addTab(skyFragmentWidget,"SkyFragment");
    mainBar->addTab(detectorWidget,"Detector");

    splitter->addWidget(leftBar);
    splitter->addWidget(mainBar);

    setCentralWidget(centralwidget);
    getCOMPorts();

//    chartDetector->show();
//    return;
#endif
//    ui->setupUi(this);


    getCOMPorts();

//    drawRadioSky();

//    RadioSkyCanvas * scene = new RadioSkyCanvas();

    canvas = new RadioSkyCanvas();
    graphicsView = new RadioSkyGraphicsView(canvas);

    radioSkyLayout -> addWidget(graphicsView);
//    radioSkyLayout -> addWidget(canvas);
    canvas -> cursorCoordinatesView = cursorCoordinatesView;
    canvas -> setCoordinatesView = setCoordinatesView;
    canvas -> comand = comandLabel;

    canvas -> sendComandLabel = sendComandLabel;
    canvas -> clearCanvas();

    fiderThread = new FiderThread();

    fiderThread->run();

    connect(this ,&MainWindow::on_pushButton_clickedS, client , &WebSocketClient::sendBinMessSlot);

    connect(client ,&WebSocketClient::stateChangedSignal, this , &MainWindow::stateChangedSlot);

    connect(client ,&WebSocketClient::setDetetorValue, this , &MainWindow::setDetetorValue);

    connect(client ,&WebSocketClient::setDetetorValue, chartDetector , &ChartDetector::addNewValToChart);

    connect(fiderThread ,&FiderThread::requestDetectorValue, client , &WebSocketClient::sendBinMessSlot);
    connect(pushButton_Scan ,&QPushButton::clicked, this , &MainWindow::on_pushButton_Scan_clicked);
    connect(comPortThread, &ComPortThread::setComandReturnStatus, this,&MainWindow::setComandReturnStatus);
    connect(graphicsView, &RadioSkyGraphicsView::sendCommandsToComPort, comPortThread,&ComPortThread::setComandReturnStatus);

    connect(graphicsView, &RadioSkyGraphicsView::setCursorCoordinatesSignal, cursorCoordinatesView,&CoordinatesView::setCoordinates);
    connect(graphicsView, &RadioSkyGraphicsView::setCoordinatesSignal, setCoordinatesView,&CoordinatesView::setCoordinates);

    OpenComPort();
}

MainWindow::~MainWindow()
{

}
void MainWindow::getCOMPorts(){
    comPortListComboBox->clear();
     auto infos = QSerialPortInfo::availablePorts();
    for (const QSerialPortInfo &info : infos) {
        QStringList list;
        list << info.portName();
      comPortListComboBox->addItem(list.first());
    }
    }
void MainWindow::delay(int time = 2)
{
    QTime dieTime= QTime::currentTime().addSecs(time);
    while (QTime::currentTime() < dieTime)
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
}
void MainWindow::OpenComPort(){
//    QString currentPortName  = ui-> comboBox_COM_port ->currentText();

    QString currentPortName  = "COM5";
     serial.setBaudRate(QSerialPort::Baud115200);
     serial.setDataBits(QSerialPort::Data8);
     serial.setParity(QSerialPort::NoParity);
     serial.setStopBits(QSerialPort::OneStop);
     serial.setFlowControl(QSerialPort::SoftwareControl);
     serial.setPortName(currentPortName);
     if (!serial.open(QIODevice::ReadWrite)) {
         qDebug() << "could open port";
          comPortStatus-> setText("Error open "+ currentPortName);
         return;
     }
     delay(2);
     startComPortThread( &serial);
     comPortStatus-> setText(currentPortName + " is open");
}
void MainWindow::sendTestData(){


     QByteArray requestData2 ="C2 ";

     requestData2[2] = 13;
     serial.write(requestData2);
     if (serial.waitForReadyRead(1000)) {
         QByteArray responseData = serial.readAll();
         while (serial.waitForReadyRead(10))
             responseData += serial.readAll();

         qDebug() << responseData;
     }
}
void MainWindow::on_pushButton_clicked()
{

//    emit on_pushButton_clickedS();
    fiderThread->run();
    //sendTestData();
}
void  MainWindow::drawRadioSky(){

//    canvas -> cursorCoordinatesView = new CoordinatesView(cursorAzimut,cursorElevation);
//    canvas -> setCoordinatesView = new CoordinatesView(setAzimut,setElevation);




}

void MainWindow::hasData(const QString &s)
{
 comandLabel->setText(s);
 Coordinates coo = Coordinates((QString)s);
 g_current_coordinates_Mutex = true;
 g_current_coordinates = coo;
 g_current_coordinates_Mutex = false;
 currentCoordinatesView->setCoordinates(coo);
 canvas->drawCoo(coo);
}

void MainWindow::on_comboBox_COM_port_activated(const QString &arg1)
{
    OpenComPort();
}
void MainWindow::startComPortThread(QSerialPort *Serialp){
    
    if(comPortThread != nullptr)
        return;
    
//    currentCoordinatesView = new  CoordinatesView(currentAzimut,currentElevation);
    comPortThread= new ComPortThread("comport",Serialp,currentCoordinatesView );
    comPortThread-> canvas = canvas;
    comPortThread ->start();

    canvas->setCOMPortThread(comPortThread) ;
    connect(comPortThread, &ComPortThread::has_data, this,&MainWindow::hasData);
    connect(comPortThread, &ComPortThread::setComandReturnStatus, this,&MainWindow::setComandReturnStatus);
     connect(graphicsView, &RadioSkyGraphicsView::sendCommandsToComPort, comPortThread,&ComPortThread::sendCommands);

}
void MainWindow::ComPortIsOpen(){
    comPortStatus -> setText("Com port is open");
}
void MainWindow::setComandReturnStatus(const QString &strr){
    commandReturn -> setText(strr);
}
void MainWindow::sendDataToComPort(QString command){
    comPortThread->sendCommands(command);
}

void MainWindow::stopScan(){
    emit scanTerminate();
}

void MainWindow::on_pushButton_Scan_clicked()
{
    scanSkyThread= new ScanSkyThread();
    scanSkyThread ->start();
    pushButton_Scan -> setText("stop");
    connect(scanSkyThread, &ScanSkyThread::startCoordinate, this,&MainWindow::sendDataToComPort);
    timer = new QTimer();
    qDebug() << connect(timer, &QTimer::timeout, scanSkyThread, &ScanSkyThread::slotTimerAlarm);
    timer->start(20000);
}
void MainWindow::stateChangedSlot(QAbstractSocket::SocketState state){
    switch (state)
       {
           case QAbstractSocket::SocketState::UnconnectedState:   qDebug() << "Status: " << "UnconnectedState";
        case QAbstractSocket::SocketState::HostLookupState:   qDebug() << "Status: " << "HostLookupState";
        case QAbstractSocket::SocketState::ConnectingState:   qDebug() << "Status: " << "ConnectingState";
        case QAbstractSocket::SocketState::ConnectedState:   qDebug() << "Status: " << "ConnectedState";
        case QAbstractSocket::SocketState::BoundState:   qDebug() << "Status: " << "BoundState";
        case QAbstractSocket::SocketState::ListeningState:   qDebug() << "Status: " << "ListeningState";
        case QAbstractSocket::SocketState::ClosingState:   qDebug() << "Status: " << "ClosingState";
       }
}
void MainWindow::setDetetorValue(double val){
//    qDebug() << "Dval uV:" << val;
    detectorValLabel -> setText(QString::number(val));
    detectorValIntLabel -> setText(QString::number((detectorValI >> 23),16));
}

void MainWindow::on_pushButton_2_clicked()
{
     chartDetector->show();
}
