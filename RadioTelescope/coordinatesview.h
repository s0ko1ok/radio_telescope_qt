#ifndef COORDINATESVIEW_H
#define COORDINATESVIEW_H

#include "coordinates.h"

#include <QFormLayout>
#include <QGroupBox>
#include <QLabel>
#include <QVBoxLayout>



class CoordinatesView  : public QGroupBox
{
    Q_OBJECT
public:
    QGroupBox *groupBox;
    QFormLayout  * mainLayout ;
    QLabel * azimutView;
    QLabel * elevationView;
    CoordinatesView(QLabel * AzimutView, QLabel * ElevationView);
    CoordinatesView(QString name);
    void setCoordinates(Coordinates coordinates);
};

#endif // COORDINATESVIEW_H
