#include "coordinates.h"
#include <QString>
#include <QDebug>
#include <cmath>
Coordinates::Coordinates()
{
    x = (double) MYNULL;
    y = (double) MYNULL;
}
Coordinates::Coordinates(int X, int Y)
{
    x = (double)X;
    y = (double)Y;
}
Coordinates::Coordinates(double X, double Y)
{
    x = X;
    y = Y;
}
Coordinates::Coordinates(QString strr)
{
    set(strr);
}
double Coordinates::getAzimut(){
    double azimutT;
    if (azimut != MYNULL)
        return azimut;
    else{

          double yx ;


         if (x==0)
             if (y > 0)
                azimutT = 0;
             else
                azimutT = 180;
         else
            azimutT = ( atan(y/x) * 90 / (3.14/2)  );
         if (x == 0)
               yx= 88;
         else
               yx= y/x;

         if (x < 0)
             if (y > 0)
                 azimutT = 90 + azimutT;
             else
                 azimutT = 90 + azimutT;
         else
             if (y < 0)
                 azimutT = 270 + azimutT;
             else
                 azimutT = 270 + azimutT;
    }
    azimut = 360 - azimutT;
    return azimut;
}
double Coordinates::getElevation(){
    if (elevation != MYNULL)
        return elevation;
    else{
         elevation =    90 - ((double) (sqrt( (double)    ((x*x) +(y*y)))));
         return elevation;
    }
}
double Coordinates::getX(){
    if (x != MYNULL)
        return x;
    else {
        x = (90 - elevation) * sin(azimutInRadians());
        return x;
    }
}
double Coordinates::getY(){
    if (y != MYNULL)
        return y;
    else {
        y = (90 - elevation) * cos(azimutInRadians());
        return y ;
    }
}
void Coordinates::set(QString strr){
//    qDebug() << strr;
    if (strr[0]=="O"){
        // OK064.8 064.8

        azimut =  strr.mid(2,5).toDouble();
        elevation =  strr.mid(8,5).toDouble();
    }
    else{
        qDebug() << "ERROR string!:" << strr;
        azimut = (double) strr.mid(3,3).toDouble();
        elevation = (double) strr.mid(9,3).toDouble();
    }
}

QString Coordinates::getString(){

//    QString string = "W" + converToQString(getAzimut()) + " " + converToQString(getElevation())+"\r\n";
//    QString string = "Q" + converToQString(getAzimut()) + " " + converToQString(getElevation()) + QChar(0x13);
    QString string = "Q" + converDoubleToQString(getAzimut()) + " " + converDoubleToQString(getElevation()) + QChar(0x13);
    qDebug() << string;
    return string;
}
QString Coordinates::converToQString(double coordinat){
    int coo = (int) coordinat;
    QString coos = QString::number(coo);
    while( coos.size()<3)
        coos = "0" + coos;
    return coos;

}
QString Coordinates::converDoubleToQString(double coordinat){

    QString coos = QString::number(coordinat, 'f', 1);
    while( coos.size()<5)
        coos = "0" + coos;
    return coos;

}
double Coordinates::azimutInRadians(){
    return azimut * 3.14 / 180;
}
