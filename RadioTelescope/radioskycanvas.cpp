#include "radioskycanvas.h"
#include <QWidget>
#include <QDebug>
#include <QMouseEvent>
#include <QLabel>
#include <QPainter>
#include <cmath>
#include "coordinates.h"


 RadioSkyCanvas::RadioSkyCanvas(QWidget *parent, Qt::WindowFlags f)
{    pixmap->fill(QColor(qRgba(255,255,255,128)));
     pixMapItem = addPixmap(*pixmap);
     painterA = new QPainter(pixmap);

     createRainBlow();
}




void RadioSkyCanvas::setCOMPortThread(ComPortThread * ComPortThread){
    comPortThread = ComPortThread;
}

void RadioSkyCanvas::clearCanvas(){

    int radius = 360;

//    pixmap->fill(QColor(Qt::green));
//    QPainter painter(&pixmap);

//    painterA = new QPainter();

//    QImage img(720, 1024,QImage::Format_RGB32);
//    painter.drawImage(0,0,img);
//    QPoint originSky =QPoint( 370,370);
    painterA->setPen(QPen(Qt::gray, 1));
    painterA->drawEllipse( originSky,360,360);
    painterA->drawEllipse( originSky,180,180);
    painterA->drawEllipse( originSky,1,1);
    painterA->drawLine(originSky.x(),originSky.y()+ radius,originSky.x(),originSky.y() - radius);
    painterA->drawLine(originSky.x()+ radius,originSky.y(),originSky.x() - radius,originSky.y());

//    painterA->drawEllipse( 100,100,10,10);
    pixMapItem = addPixmap(*pixmap);
}
void RadioSkyCanvas::drawCoo(Coordinates coo){
     uint8_t detColor = (detectorValI >> 21);
      QColor  color  = rainBlow[detColor];
     painterA->setPen(QPen(color, 1));
     painterA->drawEllipse(originSky.x()-5,originSky.y()-5,10,10);
     int x = originSky.x()+(coo.getX()*4);
     int y = originSky.y()-(coo.getY()*4);

//     qDebug() << x << " " << coo.getY()*4;
    painterA->drawEllipse(x-1,y-1,3,3);
    painterA->drawEllipse(x,y,1,1);
    pixMapItem = addPixmap(*pixmap);

}
void RadioSkyCanvas::startScanSky(){
    setAntennaDirection(0,0);
}
void RadioSkyCanvas::setAntennaDirection(double Azimut ,double Elevation){
    Coordinates setCoordinates = Coordinates( );
    setCoordinates.azimut = Azimut;
    setCoordinates.elevation = Elevation;

    QString stringg = setCoordinates.getString();
    comand->setText(stringg);
    comPortThread->putComand(stringg);
    sendComandLabel -> setText(stringg);
}
void RadioSkyCanvas::createRainBlow(){
    for (int i = 0 ; i < 256;i++){
        rainBlow[i] = qRgb(rainbow[i][0],rainbow[i][1],rainbow[i][2]);
//        rainBlow[i] = qRgb(i,i,i);
    }

}
