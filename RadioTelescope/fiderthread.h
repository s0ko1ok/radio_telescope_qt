#ifndef FIDERTHREAD_H
#define FIDERTHREAD_H
#include <QThread>

class FiderThread : public QThread
{
        Q_OBJECT
public:
    bool stop = true;
    FiderThread();
    void run();
signals:
    void requestDetectorValue();
protected:
    void timerEvent(QTimerEvent *e);
};

#endif // FIDERTHREAD_H
