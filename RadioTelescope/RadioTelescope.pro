QT       += core gui serialport websockets printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    chart_detector.cpp \
    comportthread.cpp \
    coordinates.cpp \
    coordinatesview.cpp \
    fiderthread.cpp \
    main.cpp \
    mainwindow.cpp \
    new_mainwindow.cpp \
    qcustomplot.cpp \
    radiosky_graphics_view.cpp \
    radioskycanvas.cpp \
    scanskythread.cpp \
    websocketclient.cpp

HEADERS += \
    chart_detector.h \
    comportthread.h \
    coordinates.h \
    coordinatesview.h \
    fiderthread.h \
    mainwindow.h \
    new_mainwindow.h \
    qcustomplot.h \
    radiosky_graphics_view.h \
    radioskycanvas.h \
    scanskythread.h \
    websocketclient.h

FORMS += \
    chart_detector.ui \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
