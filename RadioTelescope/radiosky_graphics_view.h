#ifndef RADIOSKYGRAPHICSVIEW_H
#define RADIOSKYGRAPHICSVIEW_H

#include "Coordinates.h"
#include "CoordinatesView.h"
#include <QGraphicsView>
#include <QObject>
#include <QMouseEvent>

class RadioSkyGraphicsView : public QGraphicsView
{
    Q_OBJECT
public:
    RadioSkyGraphicsView(QGraphicsScene * scene = nullptr, QObject *parent = nullptr) ;

    Coordinates getCoordinates(QMouseEvent *e);

    QPoint originSky =QPoint( 370,370);

    Coordinates cursorCoordinates;
    Coordinates setCoordinates;
    CoordinatesView *cursorCoordinatesView;
    CoordinatesView *setCoordinatesView;
public slots:
    void mouseMoveEvent(QMouseEvent *e);
    void mousePressEvent(QMouseEvent *e);
signals:
    void setCoordinatesSignal(Coordinates);
    void setCursorCoordinatesSignal(Coordinates);
    void sendCommandsToComPort(QString);

};

#endif // RADIOSKYGRAPHICSVIEW_H
