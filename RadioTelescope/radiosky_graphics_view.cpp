#include "radiosky_graphics_view.h"
#include <QDebug>
RadioSkyGraphicsView::RadioSkyGraphicsView(QGraphicsScene * scene,QObject *parent): QGraphicsView(scene)
{
         resize(740, 740);
         setMouseTracking(true);
}
void RadioSkyGraphicsView::mouseMoveEvent(QMouseEvent *e)
{

   cursorCoordinates = getCoordinates(e) ;
   emit setCursorCoordinatesSignal(cursorCoordinates);
};
void RadioSkyGraphicsView::mousePressEvent(QMouseEvent* e)
{
//     setCoordinates = cursorCoordinates;
     emit setCoordinatesSignal(cursorCoordinates);
     QString stringg = cursorCoordinates.getString();
//     comand->setText(stringg);
     sendCommandsToComPort(stringg);

//     comPortThread->putComand(stringg);

//     sendComandLabel -> setText(stringg);

}
Coordinates RadioSkyGraphicsView::getCoordinates(QMouseEvent *e){
    double y = ( -(e->y() - originSky.y())/4.0 );
    double x = ( e->x() - originSky.x())/4.0;
    qDebug() <<" x,y " <<  e->x() <<e->y();
    return Coordinates(x,y);
}
