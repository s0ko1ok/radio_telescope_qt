#include "mainwindow.h"
#include "websocketclient.h"

#include <QApplication>
#define ORGANIZATION_NAME "sokolov-lab.ru"
#define ORGANIZATION_DOMAIN "sokolov-lab.ru"
#define APPLICATION_NAME "RadioTelescope"
Coordinates g_current_coordinates;
bool g_current_coordinates_Mutex = false;
 WebSocketClient *client;
int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationName( "sokolov-lab.ru");
    QCoreApplication::setOrganizationDomain("sokolov-lab.ru");
    QCoreApplication::setApplicationName("RadioTelescope");
    QApplication a(argc, argv);
    client = new WebSocketClient(QUrl(QStringLiteral("ws://192.168.6.5:9002/")),true);
    MainWindow w;

    w.show();

//    QObject::connect(&client, &WebSocketClient::closed, &a, &QCoreApplication::quit);
    return a.exec();
}
