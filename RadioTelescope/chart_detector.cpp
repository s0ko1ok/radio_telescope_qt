#include "chart_detector.h"
#include "ui_chart_detector.h"
extern double detectorValD;
ChartDetector::ChartDetector(QWidget *parent) :
    QWidget(parent)

{
    mainChartDetectorLayout = new QVBoxLayout();
    setLayout(mainChartDetectorLayout);
    draw_chart();

}

ChartDetector::~ChartDetector()
{

}


void ChartDetector::draw_chart(){
       customPlot = new QCustomPlot(); // Инициализируем графическое полотно
      mainChartDetectorLayout->addWidget(customPlot);  // Устанавливаем customPlot в окно проложения
//      mainChartDetectorLayout->addWidget(new QLabel("test"));  // Устанавливаем customPlot в окно проложения

      customPlot->setInteraction(QCP::iRangeZoom,true);   // Включаем взаимодействие удаления/приближения
      customPlot->setInteraction(QCP::iRangeDrag, true);  // Включаем взаимодействие перетаскивания графика
      customPlot->axisRect()->setRangeDrag(Qt::Horizontal);   // Включаем перетаскивание только по горизонтальной оси
      customPlot->axisRect()->setRangeZoom(Qt::Horizontal);   // Включаем удаление/приближение только по горизонтальной оси
//      customPlot->xAxis->setTickLabelType(QCPAxis::ltDateTime);   // Подпись координат по Оси X в качестве Даты и Времени
//      customPlot->xAxis->setDateTimeFormat("hh:mm");  // Устанавливаем формат даты и времени

      // Настраиваем шрифт по осям координат


      // Автоматическое масштабирование тиков по Оси X
      customPlot->xAxis->setAutoTickStep(true);

      /* Делаем видимыми оси X и Y по верхней и правой границам графика,
       * но отключаем на них тики и подписи координат
       * */
      customPlot->xAxis2->setVisible(true);
      customPlot->yAxis2->setVisible(true);
      customPlot->xAxis2->setTicks(false);
      customPlot->yAxis2->setTicks(false);
      customPlot->xAxis2->setTickLabels(false);
      customPlot->yAxis2->setTickLabels(false);

      customPlot->yAxis->setTickLabelColor(QColor(Qt::blue)); // Красный цвет подписей тиков по Оси Y
      customPlot->legend->setVisible(true);   //Включаем Легенду графика
      // Устанавливаем Легенду в левый верхний угол графика
      customPlot->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignLeft|Qt::AlignTop);

      // Инициализируем график и привязываем его к Осям
      graphic = new QCPGraph(customPlot->xAxis, customPlot->yAxis);
      customPlot->addPlottable(graphic);  // Устанавливаем график на полотно
      graphic->setName("Значение детектора");       // Устанавливаем
      QPen pen = QPen(QColor(Qt::blue));
      pen.setWidth(5);
      graphic->setPen(pen); // Устанавливаем цвет графика
//      graphic->setAntialiased(false);         // Отключаем сглаживание, по умолчанию включено
//      graphic->setLineStyle(QCPGraph::lsImpulse); // График в виде импульсных тиков

      /* Подключаем сигнал от Оси X об изменении видимого диапазона координат
       * к СЛОТу для переустановки формата времени оси.
       * */
//      connect(customPlot->xAxis, SIGNAL(rangeChanged(QCPRange)),
//              this, SLOT(slotRangeChanged(QCPRange)));




      srand(15); // Инициализируем генератор псевдослучайных чисел
        time.resize(400);
        income.resize(400);
      // Заполняем график значениями
      for (int i=0; i<4; ++i)
        {
          time[i] = i;
          income[i] =0.160 + (i * 0.0001);
//          income[i] = qFabs(income[i-1]) + (i/50.0+1)*(rand()/(double)RAND_MAX-0.5);
        }

      graphic->setData(time, income); // Устанавливаем данные
      customPlot->rescaleAxes();      // Масштабируем график по данным
      customPlot->replot();           // Отрисовываем график
}

void ChartDetector::on_pushButton_clicked()
{
    customPlot->rescaleAxes();
//    addNewValToChart(detectorValD);
}
void ChartDetector::addNewValToChart(double val){
    time.append(lastIndex);
    income.append(detectorValD);
    graphic->setData(time, income); // Устанавливаем данные
    if (isAutoScale){
         customPlot->rescaleAxes();
    }
         // Масштабируем график по данным
    customPlot->replot();
    lastIndex++;
}

void ChartDetector::on_checkBox_2_stateChanged(int arg1)
{
    if (arg1){
        isAutoScale = true;
    }else
        isAutoScale = false;
}
