
#include <QtCore/QDebug>
#include "websocketclient.h"
#include <unistd.h>




#pragma pack(push, 4)
struct SimpleStruct{
    int32_t start = 0x55;
    char name[7] = "privet";
    int32_t age = 90;
    char surname[8] = "sokolov";

};
struct DetectorResponse{
    int32_t structID = 0x3;
    int32_t detectorVal = 0;
    char message[10] = "No error";


};
#pragma pack(pop)
//! [constructor]

WebSocketClient::WebSocketClient(const QUrl &url, bool debug, QObject *parent) :
    QObject(parent),
    m_url(url),
    m_debug(debug)
{
    if (m_debug)
        qDebug() << "WebSocket server:" << url;


    connect(&m_webSocket, &QWebSocket::connected, this, &WebSocketClient::onConnected);
    connect(&m_webSocket, &QWebSocket::disconnected, this, &WebSocketClient::closed);
    connect(&m_webSocket ,&QWebSocket::stateChanged, this , &WebSocketClient::stateChangedSlot);
    m_webSocket.open(QUrl(url));
}
//! [constructor]

//! [onConnected]
void WebSocketClient::onConnected()
{



    qDebug() << "WebSocket connected";
    connect(&m_webSocket,&QWebSocket::binaryMessageReceived,this,&WebSocketClient::onbinaryMessageReceived);
    connect(&m_webSocket,&QWebSocket::textMessageReceived,this, &WebSocketClient::onTextMessageReceived  );
//    while(1){
        sendBinMess();
//        usleep(10000);
//    }
    //m_webSocket.sendBinaryMessage(buff);
}

void WebSocketClient::sendBinMess(){
    struct SimpleStruct simpleStruct;
    QByteArray buff(sizeof(simpleStruct), 's');
    memcpy(buff.data(), &simpleStruct, sizeof(simpleStruct));
    m_webSocket.sendBinaryMessage(buff);
}

void WebSocketClient::sendBinMessSlot(){

    sendBinMess();
}

void WebSocketClient::onTextMessageReceived(QString message)
{   struct SimpleStruct simpleStruct;

    for (int i = 0; i < sizeof(SimpleStruct);i++){

        printf("%d %x\n", i , *(message.data() + i ));

        QChar chacha = *(message.data() + i );
        char chachacha = chacha.toLatin1();
        char * sdf = (((char*)&simpleStruct) + i ) ;
        sdf = &chachacha;
    }

    if (m_debug)
        //qDebug() << "Message received:" << message;

        printf("age!!!!!!!!!! %x\n",(int)sizeof(QChar));
        qDebug() << "message.size()!!!!!!!!" << (int)sizeof(QChar);
        qDebug() << "name" << simpleStruct.name;
        qDebug() << "surname" << simpleStruct.surname;
        qDebug() << "start" << simpleStruct.start;
         printf("start %x\n",simpleStruct.start);
        qDebug() << "Message received:" << message;
        qDebug() << "age" << simpleStruct.age;
//    m_webSocket.close();
}
//! [onTextMessageReceived]

void  WebSocketClient::onbinaryMessageReceived(const QByteArray &messadge){


//     qDebug() << "WOW!!";

     struct DetectorResponse detectorResponse;
     memcpy(&detectorResponse, messadge.data(),sizeof(detectorResponse));

//     qDebug() << "detectorVal" << detectorResponse.structID;
//     qDebug() << "detectorVal" << detectorResponse.detectorVal;
//     printf("detectorResponse.detectorVal %x \n",detectorResponse.detectorVal);
//     qDebug() << "detectorVal" << detectorResponse.message;
//     qDebug() << "detectorVal" << detectorResponse.detectorVal;




     float ref = 2.5/ ((1 << 31) - 1);
     double Dval = ref * detectorResponse.detectorVal;
//     qDebug() << "Dval uV:" << Dval;

      detectorValD = Dval ;
      detectorValI = detectorResponse.detectorVal;
    emit setDetetorValue(Dval);

}
void WebSocketClient::stateChangedSlot(QAbstractSocket::SocketState state){
    emit stateChangedSignal(state);
}
