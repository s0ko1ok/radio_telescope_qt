#include "coordinatesview.h"
#include "coordinates.h"
CoordinatesView::CoordinatesView(QString name)
{
//    groupBox = new QGroupBox(name ,this);
    setTitle(name);
    mainLayout = new QFormLayout ();
    azimutView = new QLabel("NA");
    elevationView = new QLabel("NA");
    mainLayout->addRow("Azimut:",azimutView);
    mainLayout->addRow("Elevation:",elevationView);
    setLayout(mainLayout);

}
CoordinatesView::CoordinatesView(QLabel * AzimutView, QLabel * ElevationView)
{
    azimutView =AzimutView;
    elevationView = ElevationView;
}
void CoordinatesView::setCoordinates(Coordinates coordinates){

    azimutView -> setNum(coordinates.getAzimut());
    elevationView -> setNum(coordinates.getElevation());
}
