#ifndef COMPORTTHREAD_H
#define COMPORTTHREAD_H
#include "coordinatesview.h"

#include <queue>
#include <QSerialPort>
#include <QThread>
#include <bits/stl_queue.h>
using namespace std;
class RadioSkyCanvas;
extern Coordinates g_current_coordinates;
class ComPortThread : public QThread
{
        Q_OBJECT
public:

    queue<QString> comanndQueue;
    QSerialPort *serial;
    RadioSkyCanvas * canvas;
    bool stop = true;
    CoordinatesView *coordinatesView;
    ComPortThread(QString threadName,QSerialPort *Serialp,CoordinatesView *CoordinatesView);
    void run();
    void getCoordinates();
    void putComand(QString command );
    void sendCommands(QString string);
    void setCOMPortThread(ComPortThread * ComPortThread);
    void setCoordinates(Coordinates coordinates);

    void doIt();
    QByteArray send(QByteArray br);
signals:
    void has_data(const QString &s);
    void setComandReturnStatus(QString strr);


private:
    void timerEvent(QTimerEvent *e);
    QString name;
};

#endif // COMPORTTHREAD_H
