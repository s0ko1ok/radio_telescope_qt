#ifndef WEBSOCKETCLIENT_H
#define WEBSOCKETCLIENT_H
#include <QtCore/QObject>
#include <QtWebSockets/QWebSocket>
#include <QTime>
extern double detectorValD;
extern int32_t detectorValI;
class WebSocketClient : public QObject
{
    Q_OBJECT
public:
    explicit WebSocketClient(const QUrl &url, bool debug = false, QObject *parent = nullptr);
    void sendBinMess();
public slots:
    void sendBinMessSlot();
    void stateChangedSlot(QAbstractSocket::SocketState state);
Q_SIGNALS:
    void closed();
signals:
    void stateChangedSignal(QAbstractSocket::SocketState state);
    void setDetetorValue(double val);
private Q_SLOTS:
    void onConnected();
    void onTextMessageReceived(QString message);
    void onbinaryMessageReceived(const QByteArray &message);

private:
    QWebSocket m_webSocket;
    QUrl m_url;
    bool m_debug;
};
#endif // WEBSOCKETCLIENT_H
