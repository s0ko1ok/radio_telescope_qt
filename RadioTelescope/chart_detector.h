#ifndef CHART_DETECTOR_H
#define CHART_DETECTOR_H

#include <QWidget>

#include "qcustomplot.h"
namespace Ui {
class ChartDetector;
}

class ChartDetector : public QWidget
{
    Q_OBJECT

public:
    explicit ChartDetector(QWidget *parent = nullptr);
    ~ChartDetector();
    QVBoxLayout * mainChartDetectorLayout;
    QVector <double> time;
    QVector <double>income;
    QCustomPlot * customPlot ;
     QCPGraph *graphic;
     int lastIndex = 40;
     bool isAutoScale = true;
     void draw_chart();

public slots:

     void addNewValToChart(double val);
private slots:
    void on_pushButton_clicked();

    void on_checkBox_2_stateChanged(int arg1);

private:
    Ui::ChartDetector *ui;
};

#endif // CHART_DETECTOR_H
