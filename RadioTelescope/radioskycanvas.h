#ifndef RADIOSKYCANVAS_H
#define RADIOSKYCANVAS_H
#include <QMouseEvent>
#include <QLabel>
#include <QObject>
#include <QGraphicsScene>
#include "comportthread.h"
#include "coordinates.h"
extern int32_t detectorValI;
class RadioSkyCanvas : public QGraphicsScene
{

public:
    int radius = 720;
    QPoint originSky =QPoint( 370,370);
    ComPortThread * comPortThread = nullptr;
    QLabel * comand;
    double cursorAzimutValue;
    double cursorElevationValue;
    QPixmap * pixmap = new QPixmap(740 , 740);
    QPainter *painterA;
    CoordinatesView *cursorCoordinatesView;
    CoordinatesView *setCoordinatesView;
    Coordinates cursorCoordinates;
    Coordinates setCoordinates;
    QGraphicsPixmapItem * pixMapItem;
    void setPolarCoordinates(int x, int y);
    RadioSkyCanvas(QWidget *parent=nullptr, Qt::WindowFlags f=Qt::WindowFlags());
    void mouseMoveEvent(QMouseEvent *e);

    QLabel * sendComandLabel;
    void mousePressEvent(QMouseEvent* event);
    void setCOMPortThread(ComPortThread * ComPortThread);
    Coordinates getCoordinates(QMouseEvent *e);
    void clearCanvas();
    void drawCoo(Coordinates coo);

    void startScanSky();

    void setAntennaDirection(double Azimut, double Elevation);
private:
    void createRainBlow();
    QRgb rainBlow[256];
    int rainbow[256][3] =
        {
    {0x00,0x00,0x00},{0x04,0x00,0x14},{0x08,0x00,0x28},{0x0C,0x00,0x2C},{0x0F,0x00,0x30},{0x18,0x00,0x34},{0x1E,0x00,0x38},{0x22,0x00,0x3C},
    {0x25,0x00,0x40},{0x28,0x00,0x44},{0x2B,0x00,0x48},{0x30,0x00,0x4C},{0x35,0x00,0x50},{0x38,0x00,0x54},{0x3C,0x00,0x59},{0x3F,0x00,0x5D},
    {0x42,0x00,0x63},{0x46,0x00,0x6B},{0x4C,0x00,0x72},{0x50,0x00,0x76},{0x53,0x00,0x7C},{0x56,0x00,0x81},{0x5A,0x00,0x86},{0x5E,0x00,0x8D},
    {0x63,0x00,0x94},{0x61,0x03,0x95},{0x5F,0x05,0x96},{0x5D,0x08,0x97},{0x5C,0x0A,0x98},{0x59,0x0E,0x9A},{0x56,0x11,0x9B},{0x55,0x14,0x9C},
    {0x53,0x16,0x9D},{0x51,0x19,0x9F},{0x4E,0x1D,0xA0},{0x4C,0x1F,0xA1},{0x4A,0x21,0xA2},{0x49,0x23,0xA3},{0x47,0x26,0xA4},{0x44,0x2A,0xA5},
    {0x42,0x2D,0xA6},{0x40,0x30,0xA7},{0x3E,0x32,0xA8},{0x3C,0x35,0xA9},{0x3B,0x37,0xAA},{0x38,0x3A,0xAC},{0x35,0x3E,0xAD},{0x34,0x40,0xAE},
    {0x32,0x42,0xAF},{0x30,0x44,0xB0},{0x2E,0x47,0xB1},{0x2B,0x4A,0xB2},{0x29,0x4E,0xB3},{0x28,0x50,0xB4},{0x26,0x53,0xB5},{0x24,0x55,0xB6},
    {0x22,0x57,0xB7},{0x1F,0x5B,0xB8},{0x1D,0x5F,0xBA},{0x1C,0x61,0xBB},{0x1A,0x63,0xBC},{0x18,0x65,0xBD},{0x16,0x68,0xBE},{0x14,0x6B,0xBF},
    {0x11,0x6F,0xC0},{0x0F,0x71,0xC1},{0x0D,0x74,0xC2},{0x0C,0x76,0xC3},{0x0A,0x78,0xC4},{0x04,0x7C,0xC5},{0x00,0x7F,0xC7},{0x00,0x82,0xC8},
    {0x00,0x84,0xC9},{0x01,0x86,0xC7},{0x03,0x88,0xC6},{0x07,0x8A,0xC3},{0x0C,0x8C,0xC0},{0x0F,0x8E,0xBD},{0x12,0x8F,0xBB},{0x15,0x90,0xB8},
    {0x18,0x92,0xB6},{0x1C,0x94,0xB2},{0x21,0x96,0xAF},{0x24,0x98,0xAC},{0x27,0x99,0xAA},{0x2A,0x9A,0xA8},{0x2C,0x9B,0xA6},{0x30,0x9D,0xA3},
    {0x35,0xA0,0x9F},{0x38,0xA1,0x9C},{0x3B,0xA2,0x9A},{0x3E,0xA3,0x98},{0x41,0xA5,0x96},{0x45,0xA7,0x92},{0x4A,0xA9,0x8F},{0x4D,0xAB,0x8C},
    {0x50,0xAC,0x8A},{0x54,0xAE,0x86},{0x58,0xB0,0x83},{0x5B,0xB1,0x81},{0x5E,0xB3,0x7F},{0x61,0xB4,0x7C},{0x64,0xB6,0x7A},{0x68,0xB8,0x76},
    {0x6D,0xBA,0x73},{0x70,0xBC,0x70},{0x73,0xBD,0x6E},{0x76,0xBF,0x6C},{0x79,0xC0,0x6A},{0x7D,0xC2,0x66},{0x81,0xC4,0x63},{0x84,0xC5,0x60},
    {0x87,0xC6,0x5E},{0x8A,0xC7,0x5C},{0x8D,0xC9,0x5A},{0x92,0xCB,0x56},{0x96,0xCD,0x53},{0x99,0xCE,0x51},{0x9C,0xD0,0x4E},{0x9F,0xD1,0x4B},
    {0xA2,0xD3,0x49},{0xA5,0xD5,0x46},{0xAA,0xD7,0x43},{0xAD,0xD8,0x40},{0xB0,0xDA,0x3E},{0xB3,0xDB,0x3B},{0xB6,0xDD,0x39},{0xBA,0xDF,0x35},
    {0xBF,0xE1,0x32},{0xC2,0xE2,0x30},{0xC5,0xE4,0x2E},{0xC8,0xE5,0x2B},{0xCB,0xE6,0x29},{0xCF,0xE8,0x25},{0xD4,0xEB,0x22},{0xD6,0xEC,0x20},
    {0xD9,0xED,0x1E},{0xDC,0xEF,0x1B},{0xDF,0xF0,0x19},{0xE3,0xF2,0x15},{0xE8,0xF4,0x12},{0xEB,0xF5,0x0F},{0xEE,0xF7,0x0D},{0xF1,0xF8,0x0B},
    {0xF4,0xFA,0x09},{0xF8,0xFC,0x05},{0xFD,0xFE,0x01},{0xFE,0xFD,0x02},{0xFF,0xFC,0x03},{0xFF,0xFA,0x04},{0xFF,0xF7,0x05},{0xFF,0xF3,0x06},
    {0xFF,0xEF,0x08},{0xFF,0xEC,0x09},{0xFF,0xEA,0x0A},{0xFF,0xE7,0x0B},{0xFF,0xE4,0x0C},{0xFF,0xE0,0x0E},{0xFF,0xDD,0x12},{0xFF,0xDA,0x13},
    {0xFF,0xD7,0x14},{0xFF,0xD3,0x16},{0xFF,0xCF,0x18},{0xFF,0xCC,0x1A},{0xFF,0xCA,0x1B},{0xFF,0xC8,0x1C},{0xFF,0xC5,0x1D},{0xFF,0xC1,0x1F},
    {0xFF,0xBD,0x21},{0xFF,0xBA,0x22},{0xFF,0xB8,0x24},{0xFF,0xB6,0x25},{0xFF,0xB3,0x27},{0xFF,0xB0,0x28},{0xFF,0xAB,0x2A},{0xFF,0xA9,0x2C},
    {0xFF,0xA6,0x2D},{0xFF,0xA3,0x2E},{0xFF,0xA0,0x30},{0xFF,0x9C,0x31},{0xFF,0x99,0x33},{0xFF,0x95,0x32},{0xFF,0x91,0x30},{0xFF,0x8D,0x2F},
    {0xFF,0x8A,0x2E},{0xFF,0x85,0x2C},{0xFF,0x7F,0x2A},{0xFF,0x7B,0x29},{0xFF,0x78,0x28},{0xFF,0x74,0x27},{0xFF,0x70,0x25},{0xFF,0x6B,0x24},
    {0xFF,0x66,0x22},{0xFF,0x62,0x20},{0xFF,0x5E,0x1F},{0xFF,0x5A,0x1E},{0xFF,0x57,0x1D},{0xFF,0x52,0x1C},{0xFF,0x4C,0x1A},{0xFF,0x48,0x18},
    {0xFF,0x45,0x16},{0xFF,0x41,0x15},{0xFF,0x3D,0x14},{0xFF,0x38,0x13},{0xFF,0x33,0x11},{0xFF,0x30,0x10},{0xFF,0x2B,0x0E},{0xFF,0x28,0x0D},
    {0xFF,0x24,0x0C},{0xFF,0x1F,0x0A},{0xFF,0x19,0x08},{0xFF,0x07,0x07},{0xFF,0x12,0x06},{0xFF,0x0E,0x05},{0xFF,0x0A,0x03},{0xFF,0x05,0x02},
    {0xFF,0x00,0x01},{0xFD,0x00,0x00},{0xFB,0x01,0x00},{0xF9,0x01,0x00},{0xF7,0x01,0x00},{0xF4,0x01,0x00},{0xF1,0x02,0x00},{0xEF,0x02,0x00},
    {0xEE,0x02,0x00},{0xEB,0x02,0x00},{0xE8,0x03,0x00},{0xE6,0x03,0x00},{0xE4,0x03,0x00},{0xE2,0x03,0x00},{0xE0,0x04,0x00},{0xDC,0x04,0x00},
    {0xDB,0x04,0x00},{0xD9,0x04,0x00},{0xD7,0x05,0x00},{0xD5,0x05,0x00},{0xD3,0x05,0x00},{0xD1,0x05,0x00},{0xCE,0x06,0x00},{0xCC,0x06,0x00},
    {0xCA,0x06,0x00},{0xC8,0x06,0x00},{0xC6,0x06,0x00},{0xC4,0x06,0x00},{0xC2,0x07,0x00},{0xC0,0x07,0x00},{0xBE,0x07,0x00},{0xBC,0x07,0x00},
    {0xB9,0x08,0x00},{0xB6,0x08,0x00},{0xB3,0x08,0x00},{0xB0,0x08,0x00},{0xAD,0x09,0x00},{0xA9,0x09,0x00},{0xA5,0x09,0x00},{0xA0,0x0A,0x00}};

};

#endif // RADIOSKYCANVAS_H
