#ifndef COORDINATES_H
#define COORDINATES_H

#include <QString>



#define MYNULL 666

class Coordinates
{
public:
    double x = MYNULL;
    double y  = MYNULL;
    double azimut  = MYNULL ;
    double elevation  = MYNULL ;
    double getAzimut();
    double getElevation();
    double getX();
    double getY();
    void set(QString strr);

    Coordinates();
    Coordinates(int x,int y);
    Coordinates(double X, double Y);
    Coordinates(QString strr);

    QString getString();
    QString converToQString(double coordinat);
    double azimutInRadians();

    QString converDoubleToQString(double coordinat);
};

#endif // COORDINATES_H
